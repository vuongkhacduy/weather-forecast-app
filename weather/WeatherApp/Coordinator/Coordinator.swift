//
//  CoordinatorProtocol.swift
//  ExamCoordinator
//
//  Created by vuong khac duy on 05/07/2022.
//

import UIKit

enum Event {
    case goDetailsWeatherScreen
    case goMap
}

protocol Coordinator {
    var navigationController: UINavigationController? { get set }

    func eventOccurred(with type: Event)
    func start(dataModel: WeatherDataModel?, title: String)
}

protocol Coordinating {
    var coordinator: Coordinator? {get set}
}
