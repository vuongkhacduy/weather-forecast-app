//
//  MainCoordinator.swift
//  ExamCoordinator
//
//  Created by vuong khac duy on 05/07/2022.
//

import UIKit
import CoreLocation
import MapKit

class MainCoordinator: Coordinator {
    var title: String = ""
    func start(dataModel: WeatherDataModel?, title: String) {
        data = dataModel
        self.title = title
    }
    
    var data: WeatherDataModel?
    var navigationController: UINavigationController?
    
    func eventOccurred(with type: Event) {
        switch type {
            
        case .goDetailsWeatherScreen:
            let vc = WeatherDetailsViewController()
            vc.dataModels = data
            vc.title = self.title + " " + "Weather Details"
            
            self.navigationController?.pushViewController(vc, animated: true)

        case .goMap:
            let vc = MapViewController()
//            vc.viewModel = data
            vc.title = self.title

            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func start() {
        var vc: UIViewController & Coordinating = ViewController()
        vc.coordinator = self
        vc.title = "Home"
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(red: 234.0/255.0, green: 46.0/255.0, blue: 73.0/255.0, alpha: 1.0),NSAttributedString.Key.font: UIFont(name: "Futura-Bold", size: 17)!]
        
        navigationController?.setViewControllers([vc], animated: false)
        
    }
}


