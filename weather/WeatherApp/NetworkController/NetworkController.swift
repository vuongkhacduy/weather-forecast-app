//
//  NetworkController.swift
//  WeatherApp
//
//  Created by vuong khac duy on 12/07/2022.
//

import UIKit
import CoreLocation

struct API {
    static let key = "8beab91ffa09519a2b347926d7fa9da3"

    static func checkForAPIKey() {
        precondition(API.key != "YourAPIKey", "Condition: \nEither your APIKey is invalid or you haven't filled it yet. \nPlease Fill Your APIKey")
    }
}

struct NetworkController {
    let session = URLSession(configuration: .default)
    static let shared = NetworkController()
    private static var baseUrl = "api.openweathermap.org"
    private static let apiKey = "fc67511c44f1da51c9b114fafab2b13a"

    public static var searchedCityName = ""
    public static var currentCityName = ""
    public static var timeZoneOffset = 0

    public static var current: WeatherDataModel.Current?
    public static var daily: [WeatherDataModel.Daily]?
    public static var hourly: [WeatherDataModel.Hourly]?

    static var dataModels: (WeatherDataModel)?


    //get API String
    func getAPIString(coordinate:  CLLocationCoordinate2D?) -> String {
        var urlString = ""
        if let latitude = coordinate?.latitude, let long = coordinate?.longitude {
            urlString = "https://api.openweathermap.org/data/2.5/onecall?lat=\(String(describing: latitude))&lon=\(String(describing: long))&exclude=minutely,alerts&appid=\(API.key)&units=metric"
            print(">>>>Log urlString: \(urlString) <<<>>> AT func performWeatherRequest <<<<")
        }
        return urlString
    }

    //    //func get weather with langitude and logitude
    func getWeatherWithLocationURL(coordinate:  CLLocationCoordinate2D, urlString: String, onSuccess: @escaping (WeatherDataModel) -> Void, onError: @escaping (String) -> Void) {

        guard let url = URL(string: getAPIString(coordinate:  coordinate)) else {
            onError("Error building URL")
            return
        }

        let task = session.dataTask(with: url) { (data, response, error) in

            DispatchQueue.main.async {
                if let error = error {
                    onError(error.localizedDescription)
                    return
                }

                guard let data = data, let response = response as? HTTPURLResponse else {
                    onError("Invalid data or response")
                    return
                }

                do {
                    if response.statusCode == 200 {
                        let items = try JSONDecoder().decode(WeatherDataModel.self, from: data)
                        onSuccess(items)
                    } else {
                        onError("Response wasn't 200. It was: " + "\n\(response.statusCode)")
                    }
                } catch {
                    onError(error.localizedDescription)
                }
            }
        }
        task.resume()
    }

    //get weather with city Name
    func getWeather(for cityName: String, onSuccess: @escaping (WeatherDataModel) -> Void, onError: @escaping (String) -> Void) {
        API.checkForAPIKey()

        if cityName != "" {
            CLGeocoder().geocodeAddressString(cityName) { (placemarks, error) in
                if let location = placemarks?.first?.location {
                    let coordinate = location.coordinate

                    guard let url = URL(string: getAPIString(coordinate:  coordinate)) else {
                        onError("Error building URL")
                        return
                    }

                    let task = session.dataTask(with: url) { (data, response, error) in

                        DispatchQueue.main.async {
                            if let error = error {
                                onError(error.localizedDescription)
                                return
                            }

                            guard let data = data, let response = response as? HTTPURLResponse else {
                                onError("Invalid data or response")
                                return
                            }

                            do {
                                if response.statusCode == 200 {
                                    let items = try JSONDecoder().decode(WeatherDataModel.self, from: data)
                                    onSuccess(items)
                                } else {
                                    onError("Response wasn't 200. It was: " + "\n\(response.statusCode)")
                                }
                            } catch {
                                onError(error.localizedDescription)
                            }
                        }

                    }
                    task.resume()
                }
            }
        }
    }
}
