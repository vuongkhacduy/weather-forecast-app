//
//  Extension.swift
//  WeatherApp
//
//  Created by vuong khac duy on 12/07/2022.
//

import Foundation

extension Double {
    var kelvinToFahrenheit: Double {
        return (9.0 / 5) * (self - 273) + 32
    }
    var kelvinToCelsius: Double {
        return self - 273.15
    }
    var celsiusToFahrenheit: Double {
        return self * 1.8000 + 32.00
    }
    var fahrenheitTocelsius: Double {
        return (self - 32.00) / 1.8000 
    }
}
