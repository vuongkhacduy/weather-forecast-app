//
//  MainViewModel.swift
//  WeatherApp
//
//  Created by vuong khac duy on 12/07/2022.
//

import Foundation
import CoreLocation
class MainViewModel {
    var weatherDataModel: WeatherDataModel?
    var coordinate: CLLocationCoordinate2D?

    func getWeather(searchCityText: String, completetion: @escaping () -> ()) {
        NetworkController.shared.getWeather(for: searchCityText, onSuccess: { (result) in
            self.weatherDataModel = result
            completetion()

        }) { (errorMessage) in
            debugPrint(errorMessage)
        }
    }

    func getWeatherWithLocation(coordinate: CLLocationCoordinate2D, completetion: @escaping () -> ()) {
        NetworkController.shared.getWeatherWithLocationURL(coordinate: coordinate, urlString: "") { result in
            self.weatherDataModel = result
            completetion()
        } onError: { errorMessage in
            debugPrint(errorMessage)
        }
    }
}
