//
//  ContentView.swift
//  WeatherApp
//
//  Created by vuong khac duy on 12/07/2022.
//

import UIKit
import MapKit

class ContentView: UIView {

    var viewModels = MainViewModel()
    var dataModel: WeatherDataModel?
    var isCelsius = false

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var weatherImg: UIImageView!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var countryNameLabel: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var celsiusLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var convertButton: UIButton!
    
    override init(frame: CGRect) {
        super .init(frame: frame)
        setupView()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }

    private func setupView() {
        let nib = UINib(nibName: "ContentView", bundle: nil)
        if let nibView = nib.instantiate(withOwner: self, options: nil).first as? UIView {
            self.contentView = nibView
            self.addSubview(contentView)
        }
        setupUI()
    }

    private func setupUI() {
        mapView.layer.cornerRadius = 16
        convertButton.layer.cornerRadius = 16
        isCelsius = true
        
    }

    @IBAction func convertOnClick(_ sender: Any) {
        isCelsius = !isCelsius
        if !isCelsius {
            if let celsiusToFahrenheit = self.viewModels.weatherDataModel?.current.temp.celsiusToFahrenheit {
                self.celsiusLabel.text =  "Fahrenheit: \(String(format: "%.1f", celsiusToFahrenheit))°F"
            }
        } else {
            if let fahrenheitTocelsius = self.viewModels.weatherDataModel?.current.temp {
                self.celsiusLabel.text =  "Celsius: \(String(format: "%.1f", fahrenheitTocelsius))°C"
            }
        }
        self.celsiusLabel.text = self.celsiusLabel.text
    }
    
    func reloadView(location: CLLocation, countryName: String ,dataModel: MainViewModel) {
        self.viewModels = dataModel
        // countryName
        self.countryNameLabel.text = countryName

        // Celsius label
        if let kelvinToCelsius = dataModel.weatherDataModel?.current.temp {
            self.celsiusLabel.text = "Celsius: \(String(format: "%.1f", kelvinToCelsius))°C"
        }

        // humidity label
        if let humidity = dataModel.weatherDataModel?.current.humidity {
            self.humidityLabel.text = "Humidity: \(String(format: "%d", humidity))"
        }

        //image weather
        self.weatherImg.image = UIImage(named: dataModel.weatherDataModel?.current.weather[0].icon ?? "")

        //description weather
        self.descriptionLabel.text = dataModel.weatherDataModel?.current.weather[0].description

        // current day
        self.dateLabel.text = convertDateFormat(dt: dataModel.weatherDataModel?.current.dt ?? 0)

        showOnMap(location: location)

    }

    func showOnMap(location: CLLocation )
    {
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        mapView.setRegion(region, animated: true)
    }
}
