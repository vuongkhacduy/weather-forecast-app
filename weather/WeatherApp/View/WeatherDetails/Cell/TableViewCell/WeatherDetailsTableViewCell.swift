//
//  WeatherDetailsTableViewCell.swift
//  WeatherApp
//
//  Created by Vuong Duy on 14/07/2022.
//

import UIKit

class WeatherDetailsTableViewCell: UITableViewCell {
    var dataModels: WeatherDataModel?
    var viewModel = WeatheDetailsTableViewCellModel()
    var indexOfRowTable = 0

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var maxTempLabel: UILabel!
    @IBOutlet weak var minTempLabel: UILabel!
    @IBOutlet weak var titleOfDetailsLb: UILabel!

    @IBOutlet weak var collectionViewDetails: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCollectionView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    func setupCollectionView() {
        let nib = UINib(nibName: "WeatherDetailsCollectionViewCell", bundle: nil)
        collectionViewDetails.register(nib, forCellWithReuseIdentifier: "WeatherDetailsCollectionViewCell")
        collectionViewDetails.delegate = self
        collectionViewDetails.dataSource = self
    }
}

extension WeatherDetailsTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.indexOfRowTable == 0 {
            return viewModel.dataModels?.daily.count ?? 0
        } else {
            return viewModel.dataModels?.hourly.count ?? 0
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WeatherDetailsCollectionViewCell", for: indexPath) as! WeatherDetailsCollectionViewCell

        cell.viewModels.dataModels = viewModel.dataModels

        if self.indexOfRowTable == 0 {
            cell.reloadCollectionView(indexPath: indexPath.row)
        } else {
            cell.reloadCollectionViewHourly(indexPath: indexPath.row)
        }

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150, height: 220)
    }
}
