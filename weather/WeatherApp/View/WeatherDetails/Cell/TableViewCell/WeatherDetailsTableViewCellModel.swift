//
//  WeatherDetailsTableViewCellModel.swift
//  WeatherApp
//
//  Created by vuong khac duy on 20/07/2022.
//

import Foundation
import UIKit

class WeatheDetailsTableViewCellModel {

    var dataModels: WeatherDataModel?

    var date: String = ""
    var weatherIcon = UIImage()
    var maxTemp: String = ""
    var minTemp: String = ""
    
    init() {

    }

//    func reloadCellData(indexPath: Int, completion: @escaping () -> ()) {
//        self.date = convertDateFormat(dt:  dataModels?.daily[indexPath].dt ?? 0)
//        self.weatherIcon = UIImage(named: dataModels?.daily[indexPath].weather[0].icon ?? "")!
//        if let maxTempString = dataModels?.daily[indexPath].temp.max as? CVarArg {
//            self.maxTemp = "\(String(format: "%.1f", maxTempString))°C "
//        }
//        if let minTempString = dataModels?.daily[indexPath].temp.min as? CVarArg {
//            self.minTemp = "\(String(format: "%.1f", minTempString))°C "
//        
//        }
//        completion()
//    }
}
