//
//  WeatherDetailsViewModel.swift
//  WeatherApp
//
//  Created by vuong khac duy on 29/07/2022.
//

import Foundation
import UIKit

class WeatheDetailsCollectionViewCellModel {

    var dataModels: WeatherDataModel?

    var date: String = ""
    var weatherIcon = UIImage()
    var maxTemp: String = ""
    var minTemp: String = ""

    var humidity: String = ""
    var temp: String = ""

    init() {

    }

    func reloadCellData(row: Int, indexPath: Int, completion: @escaping () -> ()) {
        setDataDaily(indexPath: indexPath)
        completion()
    }

    func reloadCellDataHourly(row: Int, indexPath: Int, completion: @escaping () -> ()) {
        setDataHour(indexPath: indexPath)
        completion()
    }


    func setDataDaily(indexPath: Int) {
        self.date = convertDateFormat(dt:  dataModels?.daily[indexPath].dt ?? 0)
        self.weatherIcon = UIImage(named: dataModels?.daily[indexPath].weather[0].icon ?? "")!
        if let maxTempString = dataModels?.daily[indexPath].temp.max as? CVarArg {
            self.maxTemp = "\(String(format: "%.1f", maxTempString))°C "
        }
        if let minTempString = dataModels?.daily[indexPath].temp.min as? CVarArg {
            self.minTemp = "\(String(format: "%.1f", minTempString))°C "

        }
    }

   func convertSecondsToHrMinuteSec(seconds:Int) -> String{
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute, .second]
        formatter.unitsStyle = .full

        let formattedString = formatter.string(from:TimeInterval(seconds))!
        print(formattedString)
        return formattedString
       }

    func setDataHour(indexPath: Int) {
        self.date = convertDateHourlyFormat(dt: dataModels?.hourly[indexPath].dt ?? 0)
        self.weatherIcon = UIImage(named: dataModels?.hourly[indexPath].weather[0].icon ?? "icon-clould-empty")!

        if let tempValue = dataModels?.hourly[indexPath].temp {
            self.temp = "\(String(describing: tempValue))°C"
        }

        if let humidityValue = dataModels?.hourly[indexPath].humidity {
            self.humidity = "\(String(describing: humidityValue))"
        }

    }
}
