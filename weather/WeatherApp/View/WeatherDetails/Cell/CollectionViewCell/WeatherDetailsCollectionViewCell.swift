//
//  WeatherDetailsCollectionViewCell.swift
//  WeatherApp
//
//  Created by vuong khac duy on 29/07/2022.
//

import UIKit

class WeatherDetailsCollectionViewCell: UICollectionViewCell {

    var viewModels = WeatheDetailsCollectionViewCellModel()

    @IBOutlet weak var detailsWeatherView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var maxTempLabel: UILabel!
    @IBOutlet weak var minTempLabel: UILabel!
    @IBOutlet weak var iconWeatherImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        detailsWeatherView.layer.cornerRadius = 16
    }

    func reloadCollectionView(indexPath: Int) {
        self.viewModels.reloadCellData(row: indexPath, indexPath: indexPath) { [weak self] in
            self?.dateLabel.text = self?.viewModels.date
            if let maxTemp = self?.viewModels.maxTemp {
                self?.maxTempLabel.text = "Max: " + maxTemp
            }
            if let minTemp = self?.viewModels.minTemp {
                self?.minTempLabel.text = "Min: " + minTemp
            }
            self?.iconWeatherImage.image = self?.viewModels.weatherIcon
        }
    }

    func reloadCollectionViewHourly(indexPath: Int) {
        self.viewModels.reloadCellDataHourly(row: indexPath, indexPath: indexPath) { [weak self] in
            self?.dateLabel.text = self?.viewModels.date
            self?.iconWeatherImage.image = self?.viewModels.weatherIcon
            if let humidityValue = self?.viewModels.humidity  {
                self?.maxTempLabel.text = "Humidity: " + humidityValue
            } else {
                self?.maxTempLabel.text = ""
            }

            if let tempValue = self?.viewModels.temp {
                self?.minTempLabel.text = "Temp: " + tempValue
            } else {
                self?.minTempLabel.text = ""
            }
        }
    }
}
