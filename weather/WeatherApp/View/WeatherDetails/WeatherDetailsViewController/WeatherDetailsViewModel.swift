//
//  WeatherDetailsViewModel.swift
//  WeatherApp
//
//  Created by vuong khac duy on 14/07/2022.
//

import UIKit

class WeatherDetailsViewModel {
    var dataModels: WeatherDataModel?
    var timeDetailsWeather = ["7 Days Forecast", "Hourly Forecast"]
    var a = ""
    var nameString = ""

    var degreeLb = ""
    var dateLb = ""
    var typeWeatherLb = ""
    var feelLikeLb = ""
    var windSpeed = ""
    var uvi = ""
    var weatherImg: UIImage = UIImage()


    init() {
    }
    
    func setupDetails(index: Int, completion: @escaping () -> ()) {
        a = timeDetailsWeather[index]
        completion()
    }

    func setupInitLayout(completion: @escaping () -> ()) {
        print(dataModels?.current)
        if let tempValue = dataModels?.current.temp {
            degreeLb = "\(String(describing: tempValue))°C"
        }

        if let currentDate = dataModels?.current.dt {
            dateLb = convertDateHourlyFormat(dt:currentDate)
        }

        if let feelsLike = dataModels?.current.feels_like {
            feelLikeLb = "Feels Like: \(String(describing: feelsLike))°C"
        }
        
        if let windSpeed = dataModels?.current.wind_speed {
            self.windSpeed = "Wind Speed: \(String(describing: windSpeed))"
        }
        if let uvi = dataModels?.current.feels_like {
            self.uvi = "Uvi: \(String(describing: uvi))"
        }

        //image weather
        if let imageWeather = UIImage(named: dataModels?.current.weather[0].icon ?? "") {
            self.weatherImg = imageWeather
        }

        //description weather
        self.typeWeatherLb = dataModels?.current.weather[0].description ?? ""


        completion()
    }

}
