//
//  WeatherDetailsViewController.swift
//  WeatherApp
//
//  Created by vuong khac duy on 14/07/2022.
//

import Foundation
import UIKit
import CoreLocation
import MapKit

class WeatherDetailsViewController: UIViewController, Coordinating {
    var coordinator: Coordinator?
    
    var viewModel =  WeatherDetailsViewModel()
    var locationManager: CLLocationManager!
    var location = CLLocation()
    private var coordinates = CLLocationCoordinate2D()
    private var locations: [CLLocation] = []

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var weatherDetailsTableView: UITableView!
    @IBOutlet weak var countryNameLb: UILabel!
    @IBOutlet weak var degreeLb: UILabel!
    @IBOutlet weak var dateLb: UILabel!
    @IBOutlet weak var typeWeatherLb: UILabel!
    @IBOutlet weak var feelLikeLb: UILabel!
    @IBOutlet weak var highTempLb: UILabel!
    @IBOutlet weak var lowTempLb: UILabel!
    @IBOutlet weak var iconWeatherImg: UIImageView!
    
    var dataModels: WeatherDataModel?
    
    override func viewWillAppear(_ animated: Bool) {
        determineMyCurrentLocation()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        weatherDetailsTableView.delegate = self
        weatherDetailsTableView.dataSource = self
        setupUI()
    }
    
    func setupUI() {
        headerView.layer.cornerRadius = 16
        weatherDetailsTableView.register(UINib(nibName: "WeatherDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "WeatherDetailsTableViewCell")

        if let locationsObj = locations.last {
            print("Current location lat-long is = \(locationsObj.coordinate.latitude) \(locationsObj.coordinate.longitude)")

            self.viewModel.dataModels?.lat = locationsObj.coordinate.latitude
            self.viewModel.dataModels?.lon = locationsObj.coordinate.longitude
            
            determineMyCurrentLocation()
        }

        self.viewModel.dataModels = dataModels
        self.viewModel.setupInitLayout {
            self.degreeLb.text = self.viewModel.degreeLb
            self.dateLb.text = self.viewModel.dateLb
            self.typeWeatherLb.text = self.viewModel.typeWeatherLb
            self.feelLikeLb.text = self.viewModel.feelLikeLb
            self.highTempLb.text = self.viewModel.windSpeed
            self.lowTempLb.text = self.viewModel.uvi
            self.iconWeatherImg.image = self.viewModel.weatherImg
        }
    }

    func getCountryFromLocation(locationsObj: CLLocation ) {
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(locationsObj) { (placemarks, error) in
            if (error != nil){
                print("error in reverseGeocode")
            }
            guard let placemarks = placemarks else {return}
            let placemark = placemarks as [CLPlacemark]
            if placemark.count>0{
                let placemark = placemarks[0]
                if let country = placemark.country, let locality = placemark.locality {
                    self.countryNameLb.text = "\(country), \(locality)"
                } else {
                    self.countryNameLb.text = ""
                }
            }
        }
    }
}

extension WeatherDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.timeDetailsWeather.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: WeatherDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "WeatherDetailsTableViewCell", for: indexPath) as! WeatherDetailsTableViewCell
        cell.viewModel.dataModels = self.viewModel.dataModels
        cell.indexOfRowTable = indexPath.row
        viewModel.setupDetails(index: indexPath.row) {
            cell.titleOfDetailsLb.text = self.viewModel.a
        }
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
}

extension WeatherDetailsViewController: CLLocationManagerDelegate {
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
            locationManager.startUpdatingHeading()
        }
    }

    // MARK: Location Manager Delegate methods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        var locationsObj = CLLocation()
        if viewModel.dataModels?.lat == 0, viewModel.dataModels?.lon  == 0 {
            locationsObj = locations.last! as CLLocation
            self.viewModel.dataModels?.lat = locationsObj.coordinate.latitude
            self.viewModel.dataModels?.lon = locationsObj.coordinate.longitude
        } else {
            locationsObj = CLLocation(latitude: viewModel.dataModels?.lat ?? 0, longitude: viewModel.dataModels?.lon ?? 0)
        }

        self.coordinates = locationsObj.coordinate
        self.location = locationsObj
        getCountryFromLocation(locationsObj: locationsObj)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Get Location failed")
    }
}
