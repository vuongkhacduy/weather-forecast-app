//
//  MapViewController.swift
//  WeatherApp
//
//  Created by vuong khac duy on 25/08/2022.
//

import Foundation
import UIKit
import MapKit

class MapViewController: UIViewController {

    var viewModel: MapViewModel!

    //MARK: Location constants
    struct cameraInfo{
        var location = CLLocationCoordinate2D()
        var heading = CLLocationDirection()
        init(latitude:CLLocationDegrees,longitude:CLLocationDegrees,heading:CLLocationDirection){
            self.location = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            self.heading = heading
        }
    }

    let wrigleyLocation = cameraInfo(latitude: 21.0284, longitude: 105.8537, heading: 41.73)
    let CPOGLocation = cameraInfo(latitude: 21.0284, longitude: 105.8537, heading: 338.0)
    let conniesLocation = cameraInfo(latitude: 21.0284, longitude: 105.8537, heading: 32.12)
    //MARK: Properties and Outlets
    //    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var mapView: MKMapView!
    //MARK: - Actions

    //MARK: Location actions

    @IBAction func gotoCPOG(sender: UIButton) {
        let camera = MKMapCamera(lookingAtCenter: CPOGLocation.location, fromDistance: 0.01, pitch: 90, heading: CPOGLocation.heading)
        mapView.setCamera(camera, animated: true)
    }

    @IBAction func gotoWrigley(sender: UIButton) {
        var eyeCoordinate = wrigleyLocation.location
        eyeCoordinate.latitude += 0.004
        eyeCoordinate.longitude += 0.004
        let camera = MKMapCamera(lookingAtCenter: wrigleyLocation.location, fromEyeCoordinate: eyeCoordinate, eyeAltitude: 50)
        mapView.mapType = .hybridFlyover
        mapView.setCamera(camera, animated: true)
    }

    @IBAction func gotoConnies(sender: UIButton) {
        let camera = MKMapCamera(lookingAtCenter: conniesLocation.location, fromDistance: 1300, pitch: 0, heading: 0.0)
        mapView.mapType = .hybrid
        mapView.setCamera(camera, animated: true)
    }

    //MARK: Appearance actions

    @IBAction func toggleMapType(sender: UIButton) {
        let title = sender.titleLabel?.text
        switch title!{
        case "Satellite":
            mapView.mapType = .satellite
            sender.setTitle("Hybrid", for: .normal)
        case "Hybrid":
            mapView.mapType = .hybrid
            sender.setTitle("Standard", for: .normal)
        case "Standard":
            mapView.mapType = .standard
            sender.setTitle("Satellite", for: .normal)
        default:
            mapView.mapType = .standard
            sender.setTitle("Sat Fly", for: .normal)
        }
    }

    @IBAction func overheadMap(sender: UIButton) {
        mapView.camera.pitch = 0.0
        mapView.camera.altitude = 1000.0
        mapView.camera.heading = 0.0
    }


    @IBAction func flyoverMap(sender: UIButton) {
        switch mapView.mapType{
        case .satellite:
            mapView.mapType = .satelliteFlyover
        case .hybrid:
            mapView.mapType = .hybridFlyover
        case .standard:
            mapView.mapType = .standard
            break
        default:
            break

        }

        let camera = MKMapCamera()
        camera.centerCoordinate = mapView.centerCoordinate
        camera.pitch = 80.0
        camera.altitude = 100.0
        mapView.setCamera(camera, animated: true)
    }


    @IBAction func toggleMapFeatures(sender: UIButton) {
        let flag = !mapView.showsBuildings
        mapView.showsBuildings = flag
        mapView.showsScale = flag
        mapView.showsCompass = flag
        mapView.showsTraffic = flag
        mapView.showsPointsOfInterest = flag
        if flag {
            sender.setTitle("Clean Map", for: .normal)
        } else {
            sender.setTitle("Busy Map", for: .normal)
        }
    }

    //MARK: Instance methods
    func setRegionForLocation(location:CLLocationCoordinate2D,spanRadius:Double,animated:Bool){
        let span = 2.0 * spanRadius
        let region = MKCoordinateRegion(center: location, latitudinalMeters: span, longitudinalMeters: span)
        mapView.setRegion(region, animated: animated)
    }

    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setRegionForLocation(location: wrigleyLocation.location, spanRadius: 150,animated: false)

        setupUI()
    }

    func setupUI() {

    }
}
