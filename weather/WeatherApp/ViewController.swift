//  ViewController.swift
//  WeatherApp
//
//  Created by vuong khac duy on 11/07/2022.
//

import UIKit
import CoreLocation
import MapKit

class ViewController: UIViewController, Coordinating {
    var coordinator: Coordinator?
    let searchController = UISearchController()
    private var models = MainViewModel()

    private var nameCountry = ""

    private var coordinates = CLLocationCoordinate2D()

    var locationManager: CLLocationManager!
    var location = CLLocation()

    @IBOutlet var tableView: UITableView!

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        determineMyCurrentLocation()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    func setupUI() {

        // background image
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background2")!)

        //right button Navigation bar
        let rightBtn = UIBarButtonItem(image: UIImage(named: "right"), style: .plain, target: self, action: #selector(didTabButtonMoveToWeatherDetailsScreen))
        rightBtn.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = rightBtn

        //left button Navigation bar
        let leftBtn = UIBarButtonItem(image: UIImage(named: "right"), style: .plain, target: self, action: #selector(didTabButtonMoveToMapScreen))
        leftBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = leftBtn

        //Search bar
        searchController.searchBar.autocapitalizationType = .none

        // For iOS 11 and later, place the search bar in the navigation bar.
        navigationItem.searchController = searchController

        // Make the search bar always visible.
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.searchBar.placeholder = "Search City"
        searchController.searchBar.tintColor = UIColor.white
        searchController.searchBar.searchTextField.textColor = UIColor.white
        searchController.searchBar.delegate = self

        setupData()
    }

    private func setupData() {
        models.getWeatherWithLocation(coordinate: self.coordinates) {
            [weak self] in
            DispatchQueue.main.async {
                if self?.nameCountry != nil, self?.nameCountry != "" {
                    self?.reloadViewAfterSearch()
                }
            }
        }
    }

    //This is click function
    @objc func didTabButtonMoveToMapScreen() {
        coordinator?.start(dataModel: models.weatherDataModel, title: "Map")
        coordinator?.eventOccurred(with: .goMap)
    }

    @objc func didTabButtonMoveToWeatherDetailsScreen() {
        coordinator?.start(dataModel: models.weatherDataModel, title: searchController.searchBar.text ?? "Weather Details")
        coordinator?.eventOccurred(with: .goDetailsWeatherScreen)
    }

    // call after finish edit search bar
    func reloadViewAfterSearch() {
        removeSubview()
        // content view details of current weather
        let contentView = UIView()
        contentView.tag = 100
        self.view.addSubview(contentView)
        contentView.frame = CGRect(x: 0, y: 120, width: self.view.frame.width, height: self.view.frame.height - 120)

        //define content UIView details of weather
        let contentWeatherView = ContentView(frame: CGRect(x: 0, y: 0, width: contentView.frame.size.width, height: contentView.frame.size.height))

        // reload content view
        contentWeatherView.reloadView(location: self.location, countryName: (searchController.searchBar.text) != "" ? searchController.searchBar.text! : self.nameCountry, dataModel: models)

        //add content weather view as subview
        contentView.addSubview(contentWeatherView)
    }

    // func remove subview before add new subview
    func removeSubview(){
        print("Start remove subview")
        if let viewWithTag = self.view.viewWithTag(100) {
            viewWithTag.removeFromSuperview()
        }else{
            print("No!")
        }
    }
}

// MARK: - UISearchBarDelegate

extension ViewController: UISearchBarDelegate {

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {

        models.getWeather(searchCityText: searchController.searchBar.searchTextField.text ?? "") {
            [weak self] in
            DispatchQueue.main.async {
                if self?.nameCountry != nil, self?.nameCountry != "" {
                    self?.reloadViewAfterSearch()
                }
            }
        }

        searchBar.resignFirstResponder()
    }
}

extension ViewController {

    func getWeather(searchCityText: String) {
        NetworkController.shared.getWeather(for: searchCityText, onSuccess: { (result) in
            self.models.weatherDataModel = result

        }) { (errorMessage) in
            debugPrint(errorMessage)
        }
    }

    func getCountry(timeZone: String) -> String {
        if let range = timeZone.range(of: "/") {
            let countryName = timeZone[range.upperBound...].trimmingCharacters(in: .whitespaces)
            print(countryName)
            return countryName
        }
        return ""
    }

    func getCountryFromLocation(locationsObj: CLLocation ) {
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(locationsObj) { (placemarks, error) in
            if (error != nil){
                print("error in reverseGeocode")
            }
            guard let placemarks = placemarks  else {return}
            let placemark = placemarks as [CLPlacemark]
            if placemark.count>0{
                let placemark = placemarks[0]

                self.nameCountry = "\(placemark.locality!), \(placemark.administrativeArea!), \(placemark.country!)"
                print(self.nameCountry)
            }
        }

    }
}

extension ViewController: CLLocationManagerDelegate {
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
            locationManager.startUpdatingHeading()
        }
    }

    // MARK: Location Manager Delegate methods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let locationsObj = locations.last! as CLLocation
        print("Current location lat-long is = \(locationsObj.coordinate.latitude) \(locationsObj.coordinate.longitude)")
        self.coordinates = locationsObj.coordinate
        self.location = locationsObj
        getCountryFromLocation(locationsObj: locationsObj)
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Get Location failed")
    }

    func showOnMap(location: CLLocation )
    {
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
//           mapView.setRegion(region, animated: true)
    }
}
