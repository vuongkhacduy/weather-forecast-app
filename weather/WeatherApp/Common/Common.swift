//
//  Common.swift
//  WeatherApp
//
//  Created by vuong khac duy on 14/07/2022.
//

import Foundation

// convet Time To NSDate
func convertTimeToNSDate(dt: Int) ->  NSDate {
    let time = NSDate(timeIntervalSince1970: TimeInterval(dt))
    return time
}

// convertDateFormat EEE, dd MMM,yyyy return String
func convertDateFormat(dt: Int) -> String {
    let dateFormatterPrint = DateFormatter()
    dateFormatterPrint.dateFormat = "EEE, dd MMM,yyyy"

    let date: NSDate? = convertTimeToNSDate(dt: dt)
    let dateString = String(dateFormatterPrint.string(from: date! as Date))
    return dateString
}

// convertDateFormat EEE, dd MMM,yyyy, hh:mm:ss return String
func convertDateHourlyFormat(dt: Int) -> String {
    let dateFormatterPrint = DateFormatter()
    dateFormatterPrint.dateFormat = "EEE, dd MMM yyyy, hh:mm:ss"

    let date: NSDate? = convertTimeToNSDate(dt: dt)
    let dateString = String(dateFormatterPrint.string(from: date! as Date))
    return dateString
}
